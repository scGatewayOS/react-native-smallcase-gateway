# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.4.0](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.3.0...v0.4.0) (2021-05-31)


### Features

* added archiveSmallcase functionality and utm params in triggerLeadGen ([314f7d1](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/314f7d10417031cdef7eaa3360fa87cbce826b5e))

## [0.3.0](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.3.0-alpha.2...v0.3.0) (2021-02-24)


### Bug Fixes

* add "no_order" error code to handle invalid pending-action orders ([89d19e4](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/89d19e44f2cc8758c6a38715837d1a6ea595e041))

## [0.3.0-alpha.2](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.3.0-alpha.1...v0.3.0-alpha.2) (2021-02-15)


### Features

* add optional list of brokers to show in trigger-transaction (ios) ([f1f0eba](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/f1f0ebaa98ceffbac55df2a791de9bc199fc4597))

## [0.3.0-alpha.1](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.3.0-alpha.0...v0.3.0-alpha.1) (2021-02-10)


### Bug Fixes

* add null safety to trigger-transaction ([d42d1ba](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/d42d1ba8d6c7ef80d6c7b115e0f4d55de0cb20f8))

## [0.3.0-alpha.0](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.2.0...v0.3.0-alpha.0) (2021-02-10)


### Features

* add optional list of brokers to show in trigger-transaction ([d0c0036](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/d0c003600766991a8df7e8a87cc4f46c75878b2c))

## [0.2.0](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.1.1-alpha.0...v0.2.0) (2021-01-05)


### Features

* add logout user and signup key in ios ([8cb5b74](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/8cb5b74e2b402253c4e2bfff2886c7e669df8f3b))


### Bug Fixes

* remove json handling in ios, and pass the response as it is ([67ed038](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/67ed038bf873180fc7ca394a4fba514e83bee66c))

### [0.1.1-alpha.0](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.1.0...v0.1.1-alpha.0) (2020-12-30)


### Features

* add logout user ([e7ac74f](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/e7ac74fddc2dfe808d8403621fc9a65cc22b8e49))

## [0.1.0](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.29...v0.1.0) (2020-12-09)

### [0.0.29](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.28...v0.0.29) (2020-12-04)

### [0.0.28](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.27...v0.0.28) (2020-12-04)


### Bug Fixes

* fix null being taken as object ([2aaefed](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/2aaefedaf79c52532219dbd8a25726a888b9d280))

### [0.0.27](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.26...v0.0.27) (2020-11-23)

### [0.0.26](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.25...v0.0.26) (2020-11-19)


### Bug Fixes

* ignore postinstall error ([f4918db](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/f4918db3c12db65a3d4942644e6e1b627a4a2b4d))

### [0.0.25](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.24...v0.0.25) (2020-11-19)

### [0.0.24](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.23...v0.0.24) (2020-11-19)

### [0.0.23](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.22...v0.0.23) (2020-11-19)

### [0.0.22](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.21...v0.0.22) (2020-11-13)

### Features

- hardcode public maven creds for smallcase repo ([572433d](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/572433deed0ded099dfe5f5096f1feb2720eaf27))

### Bug Fixes

- add missing "order_pending" error msg ([c547f12](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/c547f126677bdf3c28c8fa155bf5655f42f9aae7))
- make default min sdk version 21 ([baae752](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/baae75233426dfbcd6a8aa7b4c621f31ffcc37bd))

### [0.0.21](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.20...v0.0.21) (2020-11-11)

### Features

- add test framework (jest) ([3e7ed08](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/3e7ed08d0aa1e2d92224eff346016fe352aa644b))

### Bug Fixes

- add constants.js in npm files ([09cdca8](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/09cdca8cf45d1d2575606cb165921f629d04e4bd))

### [0.0.20](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.19...v0.0.20) (2020-11-11)

### [0.0.19](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/compare/v0.0.18...v0.0.19) (2020-11-11)

### 0.0.18 (2020-11-11)

### Features

- add commitizen ([c901563](https://gitlab.com/scGatewayOS/react-native-smallcase-gateway/commit/c901563b1bedcd7de4c9839b1e4aa720a076f61c))
